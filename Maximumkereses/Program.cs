﻿using System;

namespace Maximumkereses
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfElements = 15;
            int minimumValue = 0;
            int maximumValue = 100;
            
            int[] elements = new int[numberOfElements];
            Random r = new Random();

            Console.WriteLine("--- Tömbgenerálás ---");
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i] = r.Next(minimumValue, maximumValue + 1);
                Console.WriteLine("A(z) {0,3}. elem értéke: {1,3}", i + 1, elements[i]);
            }
            Console.WriteLine();

            Console.WriteLine("--- Maximumkeresés ---");
            int maximumElement = elements[0];
            for (int i = 1; i < elements.Length; i++)
            {
                if (elements[i] > maximumElement)
                {
                    maximumElement = elements[i];
                }
            }
            Console.WriteLine("A legnagyobb elem értéke: {0}", maximumElement);

            Console.WriteLine("\nA kilépéshez kérem nyomjon meg egy billentyűt!");
            Console.ReadKey();
        }
    }
}